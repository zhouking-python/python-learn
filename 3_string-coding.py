#!/usr/local/bin/python3
#coding=utf-8
#title:字符串和编码
#读取、保存本地文件 mem	 -------------	vi	-------------- test.txt
#读取		code	Unicode ----to---->	Utf-8 -----------> Utf-8
#保存		code	Unicode <---to----  to  <------------- utf-8
#___________________________________________________________________
#浏览网页			server -----------  client -----------> browser
#读取网页	code    Unicode ----to---->  Utf-8 -----------> Utf-8
#写入数据	code	Unicode <---to-----  Utf-8 <----------- Utf-8 
print('包含中文的str')
 #ord()函数获取字符的整数编码表示,chr()函数把编码转换为对应字符
print(ord('A'),ord('中'),chr(66),chr(25991),'\u4e2d\u6587')
#'\u4e2d\u6587' 当知道字符的整数编码时，可以直接用十六进制表示出字符
x = b'ABC'
#b'ABC' &ABC c的区别在于b'ABC' bytes的每个字符都只占用一个字节,'ABC'是srting字符串.
print(x,'ABC')
#Unicode的srt通过encode()方法编码为的bytes，纯英文的str可以用ASCII编码为bytes，内容不变
print('ABC'.encode('ascii'))
#含有中文的str可以用UTF-8编码为bytes。
print('中文'.encode('utf-8'))
#含有中文的str无法用ASCII编码，中文编码范围超过了ASCII编码范围。bytes中，无法显示为ASCII字符的字节，用\x##显示
print('中文'.encode('ascii'))
#decode()函数能将网络或者磁盘上读取的字节流bytes--->str.
print(b'ABC'.decode('ascii'))
#\x##模式表示的中文的字节码,通过decode()转换为utf-8展示。
print(b'\xe4\xb8\xad\xe6\x96\x87'.decode('utf-8'))
#len()函数计算的是str的字符数，如果换成bytes，len()函数就计算字节数
print(len('ABC'),len('中文'),len(b'ABC'),len(b'\xe4\xb8\xad\xe6\x96\x87'),len('中文'.encode('utf-8')))

#占位符%d--整数, %f--浮点数, %s--字符串,%x--十六进制整数
print('Hello, %s' % 'world')
#格式化整数和浮点数还可以指定是否补0和整数与小数的位数
print('%2d-%02d' % (3, 1))	#%2d-->默认补齐为空格数量含原字符为2,%02d-->指定补齐字符为0，数量含原字符总为2.
print('%.2f' % 3.1415926)	#%.2f-->表示小数点后两位
print('Age: %s. Gender: %s' % (25, True)) #%s永远起作用
print('growth rate: %d %%' % 7) 	#%为普通字符%%来解决

#练习题：
#小明的成绩从去年的72分提升到了今年的85分，请计算小明成绩提升的百分点，并用字符串格式化显示出'xx.x%'，只保留小数点后1位：
s1 = 72
s2 = 85
r = (s2 - s1)/s1*100
print('%2.1f%%' % r)
