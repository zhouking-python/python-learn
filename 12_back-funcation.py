#!/usr/local/bin/python3
#返回函数:函数除了做为参数以外，还可以作为结果值返回.
def calc_sum(*args):		#calc_sum为一个求和函数,取得传入参数的所有和
    ax = 0
    for n in args:
        ax = ax + n
    return ax

#闭包（Closure）的程序结构
def lazy_sum(*args):		#将函数sum封装在lazy_sum函数里面,lazy_sum被调用时不会立即求和
    def sum():			#而是求和函数
        ax = 0
        for n in args:
            ax = ax +n
        return ax
    return sum

f = lazy_sum(1, 3, 5, 7, 9)     #f为一个函数
f()				#调用函数f()才能真正取得值

f1 = lazy_sum(1, 3, 5, 7, 9)
f2 = lazy_sum(1, 3, 5, 7, 9)
f1 == f2
#False		f1 f2调用结果互不影响

#闭包（Closure）
def count():
    fs = []
    for i in range(1, 4):
        def f():
             return i*i
        fs.append(f)		#每次循环都新建了一个函数,然后将三个函数连接组成list
    return fs

f1, f2, f3 = count()		#函数值都一样，因为都引用了变量i执行到了最后

def count():
    def f(j):			#待研究
        def g():
            return j * j
        return g
    fs = []
    for i in range(1,4):
        fs.append(f(i))
    return fs

f1, f2, f3 = count()


def count():
    def f(j):
        return lambda: j*j	#待研究
    fs = []
    for i in range(1,4):
        fs.append(f(i))
    return fs

f1, f2, f3 = count()


#匿名函数:当传入函数时不需要显示的定义函数,直接传入匿名函数更方便.
list(map(lambda x: x * x, [1, 2, 3, 4, 5, 6, 7, 8, 9])) #lambda x: x * x = def f(x): return x * x

f = lambda x: x * x	#匿名函数也是一个对象也可以赋值给一个变量,在利用变量来调用函数
f(5)

def build(x, y):	#将匿名函数作为返回值
    return lambda: x * x + y * y


#装饰器:在函数调用前后自动打印日志,但又不希望修改now()函数的定义,这种在代码运行期间动态增加功能的方式,称之为'装饰器'(Decorator)
#函数也是一个对象,而且函数对象可以被赋值给变量,所以,通过变量也能调用该函数.
def now():
    print('2017-05-01')
f = now
f()

now.__name__		#函数对象有一个__name__属性
f.__name__		


def log(func):					#定义log()装饰器,要传入的参数为要执行的函数如now()
    def wrapper(*args, **kw):			#wrapper函数参数定义为(*args, **kw)可以接受任意 参数的调用.此处会改变调用函数的名称
        print('call %s():' % func.__name__)	#定义语句,日志打印内容.
        return func(*args, **kw)		#调用原始函数.
    return wrapper				#log()返回的是一个wrapper函数.

@log						#相当于now = log(now)
def now():
    print('2017-05-01')

#如果decorator本身需要传入参数,那就需要编写一个返回decorator的高阶函数。比如，要自定义log的文本：
def log(text):					#定义log()函数
    def decorator(func):			#定义decorator装饰器，传入的参数为(要调用的函数如now)
        def wrapper(*args, **kw):		#wapper函数参数定义为(*args, **kw)可以接受任意 参数的调用. 此处会改变调用函数的名称为wrapper
            print('%s %s():' %(text, func.__name__)) 	#定义日志打印内容此处调用了log()传入的test
            return func(*args, **kw)		#调用原始函数
        return wrapper				#返回函数主体
    return decorator				#log函数返回的是decorator

@log('execute')					#此处传入参数用于实现自定义日志,now = log('execute')(now)
def now():
    print('2015-3-25')


#导入functools模块解决函数名背修改的问题: 无参
import functools

def log(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        print('call %s():' % func.__name__)
        return func(*args, **kw)
    return wrapper


#带参
import functools
def log(text):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kw):
            print('%s %s():' % (text, func.__name__))
            return func(*args, **kw)
        return wrapper
    return decorator

#在函数开头和执行完都打印相应的日志:
import functools
def log(text):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kw):
            print('begin %s %s():' % (text, func.__name__))
            res = func(*args, **kw)		#函数执行结尾时增加打印日志
            print('end %s %s():' % (text, func.__name__))
            return res
        return wrapper
    return decorator

@log('execute')                                 #此处传入参数用于实现自定义日志,now = log('execute')(now)
def now():
    print('2015-3-25')

#能同时适应加参和不加参的函数
import functools
def log(text):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kw):
            print('begin %s %s():' % (text, func.__name__))
            res = func(*args, **kw)
            print('end %s %s():' % (text, func.__name__))
            return res
        return wrapper
    if text != '':
        print(text[0])
    return decorator

@log('execute')       
def now():
    print('2015-3-25')




#偏函数:functools模块提供了很多有用的功能，其中一个就是偏函数(Partial function)
int('12345')		#将字符串作为10进制转换为10进制的整数,默认为10进制
int('12345', base=8)	#将字符串作为8进制转换为10进制整数 通过base=8 定义
int('12345', 16)	#作为16进制转换为10进制整数
#要转换大量的二进制字符串，每次都传入int(x, base=2)非常麻烦
def int2(x, base=2):
    return(x, base)

int2('1000000')

#functools.partial就是帮助我们创建一个偏函数的，不需要我们自己定义int2()
import functools
int2 = functools.partial(int, base=2)	#把一个函数的某些参数固定,设置为默认值,但是也可以传参为别的值.

int2('1000000')

int2('1000000', base=10)		

#创建偏函数时，实际上可以接收函数对象、*args和**kw这3个参数传入
int2 = functools.partial(int, base=2)	#int()函数固定住了base关键字
int2('10010') 				#相当于kw = {'base': 2} int('10010', **kw)

max2 = functools.partial(max, 10)
max2(5, 6, 7)				#会将10作为*args的一部分自动添加到左边进行比较

args = (10, 5, 6, 7)
max(*args)

