#!/usr/local/bin/python3
#coding=utf-8
#function  
#1.内置函数 abs  sbsolute value of the argument

print(abs(100))
print(abs(-20))

#max 
print(max(1, 2))
print(max(2, 3, 1, -5))

#数据类型转换 int() 转换为整数
print(int('123'))
print(int(12.34))
print(float('12.34'))  	#float 转换为浮点
print(str(1.23))		#str转换为字符串
print(bool(1)) 			#转换为bool true
print(bool(''))			#转换为bool false	

#函数名其实就是指向一个函数对象的引用，完全可以把函数名赋给一个变量，相当于给这个函数起了一个“别名”
a=abs
print(a(-1))

### 练习题 :请利用Python内置的hex()函数把一个整数转换成十六进制表示的字符串
n1 = 255
n2 = 1000
print(str(hex(n1)),str(hex(n2)))

#2.定义函数 def 绝对值操作

abstest.py
def my_abs(x):
    if x >= 0:
        return x
    else:
        return -x

print(my_abs(-5))

from abstest import my_abs

#空函数，啥也不干
def nop():
    pass

#pass 占位符号
if age >= 18:
    pass

#定义检查参数的类型 进行绝对值运算
def my_abs(x):
    if not isinstance(x, (int, float)):
        raise TypeError('bad operand type')
    if x >= 0:
        return x
    else:
        return -x

#返回多个值
import math

#给出坐标、位移和角度，就可以计算出新的新的坐标
def move(x, y, step, angle=0):
    nx = x + step * math.cos(angle)
    ny = y - step * math.sin(angle)
    return nx, ny


x, y = move(100, 100, 60, math.pi / 6)
print(x, y)

#但其实这只是一种假象，Python函数返回的仍然是单一值,为一个元组
r = move(100, 100, 60, math.pi / 6)
print(r)


#定义函数时，需要确定函数名和参数个数;如果有必要，可以先对参数的数据类型做检查;函数体内部可以用return随时返回函数结果;函数执行完毕也没有return语句时，自动return None;函数可以同时返回多个值，但其实就是一个tuple。


#练习题:请定义一个函数quadratic(a, b, c)，接收3个参数，返回一元二次方程:ax2 + bx + c = 0的两个解。计算平方根可以调用math.sqrt()函数：

import math
def quadratic(a, b, c):
	if not isinstance(a,(int,float)):
		raise TypeError('bad operand type')
	elif not isinstance(b,(int,float)):
		raise TypeError('bad operand type')
	elif not isinstance(c,(int,float)):
		raise TypeError('bad operand type')
	elif (a == 0 or b == 0 or c == 0):
		raise TypeError('a b c can not be 0')
	elif (b * b) < (4 * a * c):
		raise TypeError('b*b must >= 4*a*c')
	else:
		x1=((-b + math.sqrt(b * b - 4 * a * c))/(2 * a))
		x2=((-b - math.sqrt(b * b - 4 * a * c))/(2 * a))
		return x1, x2


print(quadratic(2, 3, 1))


#3.函数的参数
#定义函数的时候，我们把参数的名字和位置确定下来，函数的接口定义就完成了。
#位置参数
def power(x):			#对于power(x)函数而言，参数x就是一个位参
	return x * x


print(power(5))


def power(x, n):
	s = 1
	while n > 0:		#此循环从n一直执行到n=0，x * x * .... * x
		n = n -1
		s = s * x
	return s


print(power(5, 2))


#默认参数
def power(x, n=2):  	#默认设置n=2 当传入的值不含n的位参时使用默认值
    s = 1
    while n > 0:
        n = n - 1
        s = s * x
    return s

print(power(5))


def enroll(name, gender):	#本函数被调用时只能允许传入两个参数
    print('name:', name)
    print('gender:', gender)

print(enroll('Sarah', 'F'))


def enroll(name, gender, age=6, city='Beijing'):	#要继续传入年龄、城市等信息，可以把年龄和城市设为默认参数
    print('name:', name)
    print('gender:', gender)
    print('age:', age)
    print('city:', city)


print(enroll('Sarah','F'))		#会加入默认信息年龄城市

enroll('Bob', 'M', 7)					#与默认参数不符的学生才需要提供额外的信息：
enroll('Adam', 'M', city='Tianjin')		#当不按顺序提供部分默认参数时，需要把参数名写上


def add_end(L=[]):				#此函数多次运行默认值会出现异常，因为对象可变
    L.append('END')
    return L

print(add_end())

print(add_end())


def add_end(L=None):			#默认参数必须指向不变对象
    if L is None:
        L = []
    L.append('END')
    return L

###为什么要设计str、None这样的不变对象呢？因为不变对象一旦创建，对象内部的数据就不能修改，这样就减少了由于修改数据导致的错误。此外，由于对象不变，多任务环境下同时读取对象不需要加锁，同时读一点问题都没有。我们在编写程序时，如果可以设计一个不变对象，那就尽量设计成不变对象。



#可变参数:传入的参数数量可变 可变参数允许你传入0个或任意个参数，这些可变参数在函数调用时自动组装为一个tuple

#请计算a2 + b2 + c2 + ……
def calc(numbers):			#numbers需要定义为list或者tuple
    sum = 0
    for n in numbers:
        sum = sum + n * n
    return sum

print(calc([1, 2, 3, 4]))
print(calc((1, 3, 5, 7)))


def calc(*numbers):			#我们把函数的参数改为可变参数,在前面加入*
    sum = 0
    for n in numbers:
        sum = sum + n * n
    return sum

print(cacl(1, 2)) 

nums = [1, 2, 3]
print(calc(nums[0], nums[1], nums[2]))
print(calc(*nums))			#*表示将列表转换为元组直接传入可变参数至函数


#关键字参数:而关键字参数允许你传入0个或任意个含参数名的参数，这些关键字参数在函数内部自动组装为一个dict。
def person(name, age, **kw):	#** 表示关键字参数
    print('name:', name, 'age:', age, 'other:', kw)

print(person('Michael', 30))
##name: Michael age: 30 other: {}
print(person('Bob', 35, city='Beijing'))
##name: Bob age: 35 other: {'city': 'Beijing'}
print(person('Adam', 45, gender='M', job='Engineer'))
##name: Adam age: 45 other: {'gender': 'M', 'job': 'Engineer'}

##先组装出一个dict，然后，把该dict转换为关键字参数传进去
extra = {'city': 'Beijing', 'job': 'Engineer'}
print(person('Jack', 24, city=extra['city'], job=extra['job']))
#name: Jack age: 24 other: {'city': 'Beijing', 'job': 'Engineer'}
print(person('Jack', 24, **extra))  # ** 表示将后面的数据字典所有key-value用关键字参数传入到函数**kw的参数中
##注意kw获得的dict是extra的一份拷贝，对kw的改动不会影响到函数外的extra


#命名关键字参数:函数的调用者可以传入任意不受限制的关键字参数。至于到底传入了哪些，就需要在函数内部通过kw检查。

def person(name, age, **kw): 	#希望检查city job参数，其他关键字参数不限制
    if 'city' in kw:
        ## 有city参数
        pass
    if 'job' in kw:
        ## 有job参数
        pass
    print('name:', name, 'age:', age, 'other:', kw)

print(person('Jack', 24, city='Beijing', addr='Chaoyang', zipcode=123456))

def person(name, age, *, city, job):  #命名关键字参数分隔符为*，*后面的参数被视为命名关键字参数。
    print(name, age, city, job)

print(person('Jack', 24, city='Beijing', job='Engineer'))

def person(name, age, *args, city, job):		#如果函数定义中已经有一个可变参数，后面跟着命名关键字参数就不再需特殊分隔符*
    print(name, age, args, city, job)

def person(name, age, *, city='Beijing', job):	#命名关键字参数可以有缺省值
    print(name, age, city, job)

print(person('Jack', 24, job='Engineer'))		#可以不输入city参数
##使用命名关键字参数时，要特别注意，如果没有可变参数，就必须加一个*作为特殊分隔符。

#参数组合:在Python中定义函数，可以用必选参数、默认参数、可变参数、关键字参数和命名关键字参数，这5种参数都可以组合使用。但是请注意，参数定义的顺序必须是：必选参数、默认参数、可变参数、命名关键字参数和关键字参数。

def f1(a, b, c=0, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)

def f2(a, b, c=0, *, d, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'd =', d, 'kw =', kw)

#在函数调用的时候，Python解释器自动按照参数位置和参数名把对应的参数传进去。

f1(1, 2)
#a = 1 b = 2 c = 0 args = () kw = {}
print(f1(1, 2, c=3))
#a = 1 b = 2 c = 3 args = () kw = {}
print(f1(1, 2, 3, 'a', 'b'))
#a = 1 b = 2 c = 3 args = ('a', 'b') kw = {}
print(f1(1, 2, 3, 'a', 'b', x=99))
#a = 1 b = 2 c = 3 args = ('a', 'b') kw = {'x': 99}
print(f2(1, 2, d=99, ext=None))
#a = 1 b = 2 c = 0 d = 99 kw = {'ext': None}
#最神奇的是通过一个tuple和dict，你也可以调用上述函数：

args = (1, 2, 3, 4)
kw = {'d': 99, 'x': '#'}
print(f1(*args, **kw))
#a = 1 b = 2 c = 3 args = (4,) kw = {'d': 99, 'x': '#'}
args = (1, 2, 3)
kw = {'d': 88, 'x': '#'}
print(f2(*args, **kw))
#a = 1 b = 2 c = 3 d = 88 kw = {'x': '#'}
#所以，对于任意函数，都可以通过类似func(*args, **kw)的形式调用它，无论它的参数是如何定义的。

#Python的函数具有非常灵活的参数形态，既可以实现简单的调用，又可以传入非常复杂的参数。
#默认参数一定要用不可变对象，如果是可变对象，程序运行时会有逻辑错误！
#要注意定义可变参数和关键字参数的语法：
#*args是可变参数，args接收的是一个tuple；
#**kw是关键字参数，kw接收的是一个dict。
#以及调用函数时如何传入可变参数和关键字参数的语法：
#可变参数既可以直接传入：func(1, 2, 3)，又可以先组装list或tuple，再通过*args传入：func(*(1, 2, 3))；
#关键字参数既可以直接传入：func(a=1, b=2)，又可以先组装dict，再通过**kw传入：func(**{'a': 1, 'b': 2})。
#使用*args和**kw是Python的习惯写法，当然也可以用其他参数名，但最好使用习惯用法。
#命名的关键字参数是为了限制调用者可以传入的参数名，同时可以提供默认值。
#定义命名的关键字参数在没有可变参数的情况下不要忘了写分隔符*，否则定义的将是位置参数。




##递归函数:函数内部在调用自己本身,则是递归函数.
def fact(n):
    if n==1:
        return 1
    return n * fact(n - 1)

print(fact(5))

##理论上，所有的递归函数都可以写成循环的方式，但循环的逻辑不如递归清晰。

def fact(n):
    a = 1
    while n >= 1:
        a = n * a
        n = n - 1
    return a

print(fact(5))

##递归函数应当防止栈溢出，通过尾递归优化，类似循环。
def fact(n):
    return fact_iter(n, 1)

def fact_iter(num, product):
    if num == 1:
        return product
    return fact_iter(num - 1, num * product)

print(fact(5))

#汉诺塔的移动:move(n, a, b, c)函数,它接收参数n,表示3个柱子A、B、C中第1个柱子A的盘子数量,然后打印出把所有盘子从A借助B移动到C的方法
def move(n, a, b, c):

def move(n, a, b, c):
    if n == 1:
        print(a, '-->', c)
    else:
        move(n-1, a, c, b)
        move(1, a, b, c)
        move(n-1, b, a, c)
#如果只有一块的话，就 A -- C
#如果有两块的话，则需要以 B 作为缓冲，也就是先将 A -- B， 然后剩下一块，进行 A -- C （此时进入一块的递归）

#如果有三块,则把上边两块看为一个整体,以B作为缓冲,进行 A -- B （此时进入递归，也就是两块的时候，进行 A -- C 的移动（此时目的是移动到B 而不是C， 所以进入新的递归 A -- B ））。此时两块整体在B，底块在A，所以进行 A -- C 的移动。然后再将两块的整体进行拆分，也就是 B 通过 A 缓冲 进入C。也就是实际执行 B -- C 的移动。
#上边两块为整体，也就是以 n-1 块进行 A -C- B, 然后 A -- C， 然后再以 n-1 块为整体进行 B -A- C。
move(3, 'A', 'B', 'C')



