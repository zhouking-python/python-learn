#!/usr/local/bin/python3
#coding=utf-8
#title:使用list和tuple
#list和tuple是Python内置的有序集合，一个可变，一个不可变。根据需要来选择使用它们。
#2017-04-09
classmates = ['Michael','Bob','Tracy']
print (classmates)
len(classmates)
print(classmates[0])
print(classmates[1])
print(classmates[2])
print(len(classmates) -1)  #谨防索引越界
print(classmates[-1])
print(classmates[-2])
print(classmates[-3])
print(classmates[0:len(classmates) - 1])  #谨防索引越界
print(classmates.append('admin'))		  #append 函数添加一个元素进入集合最后
print(classmates)
classmates.insert(1,'Jack')				  #将Jack元素添加到指定的索引号为1的位置
print(classmates)
classmates.pop()						  #pop函数默认删除集合中最后一个元素
print(classmates)
classmates.pop(1)						  #删除集合中索引位置为1的元素
print(classmates)
classmates[1] = 'Sarah'					  #将集合中索引位置为1的元素替换为Sarah
print(classmates)
L = ['Apple', 123, True]
s = ['python','java',['asp','php'],'scheme']
print(len(s))
p = ['asp','php']
s = ['python','hava',p,'scheme']		  #p变量被引用
S = ['python','hava','p','scheme']		  #p为字符
print(p)
print(S)
L = []
print(len(L))

												
classmates = ('Michael', 'Bob', 'Tracy')  #tuple小括号定义的几何中的元素不可变
dir(classmates)
t = (1, 2)
print(t)
t = ()
print(t)
t = (1)
print(t)
t = (1,)
print(t)
t = ('a', 'b', ['A', 'B'])				  #tuple小括号定义的几何中的元素不可变，但下面元素中含有集合可变
t[2][0] = 'X'
t[2][1] = 'Y'
print(t)



############练习题################
L = [
    ['Apple', 'Google', 'Microsoft'],
    ['Java', 'Python', 'Ruby', 'PHP'],
    ['Adam', 'Bart', 'Lisa']
]

######打印Apple
print(L[0][0])


######打印python
print(L[1][1])

######打印lisa
print(L[2][2])
