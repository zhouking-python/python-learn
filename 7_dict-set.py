#!/usr/local/bin/python3
#coding=utf-8
#dictionary  key-value 存储 数据字典
#list 实现保存名字对应成绩
names = ['Michael', 'Bob', 'Tracy']
sources = [95, 75, 85]

name = 'Bob'
for i in range(len(names)):
	if names[i] == name:
		print(name, 'sources is',sources[i])
	else:
		i = i + 1

#字典定义方式，初始化指定
d = {'Michael': 95, 'Bob': 75, 'Tracy': 85}
print(d['Michael'])

#通过key放入
d['Adam'] = 67
print(d['Admin'])
#一个key只能对应一个value，所以，多次对一个key放入value，后面的值会覆盖前面的值
d['Jack'] = 90
print(d['Jack'])
d['Jack'] = 88
print(d['Jack'])
#取得不存在key时,dict会报错,避免key不存在的错误,一是通过in判断key是否存在,二是通过get方法，如果key不存在，可以返回None，或者自己指定的value
d['Thomas']
print('Thomas' in d)
print(d.get('Thomas', -1))
#删除一个key
d.pop('Bob')


#和list比较，dict有以下几个特点：

#1.查找和插入的速度极快，不会随着key的增加而变慢；
#2.需要占用大量的内存，内存浪费多。
#而list相反：

#1.查找和插入的时间随着元素的增加而增加；
#2.占用空间小，浪费内存很少。

#dict的key必须是不可变对象。因为dict根据key来计算value的存储位置，如果每次计算相同的key得出的结果不同，那dict内部就完全混乱了。这个通过key计算位置的算法称为哈希算法（Hash）。
#要保证hash的正确性，作为key的对象就不能变。在Python中，字符串、整数等都是不可变的，因此，可以放心地作为key。而list是可变的，就不能作为key：

#set
#set和dict类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在set中，没有重复的key。
#要创建一个set，需要提供一个list作为输入集合
s = set([1, 2, 3])
print(s)
#重复元素在set中自动被过滤:
s = set([1, 1, 2, 2, 3, 3])
print(s)
#通过add(key)方法可以添加元素到set中，可以重复添加，但不会有效果:
s.add(3)
print(s)
#通过remove(key)方法可以删除元素:
s.remove(3)
print(s)
#set可以看成数学意义上的无序和无重复元素的集合，因此，两个set可以做数学意义上的交集、并集等操作
s1 = set([1, 2, 3])
s2 = set([2, 3, 4])
print(s1 & s2)
print(s1 | s2)
#set和dict的唯一区别仅在于没有存储对应的value，但是，set的原理和dict一样，所以，同样不可以放入可变对象，因为无法判断两个可变对象是否相等，也就无法保证set内部“不会有重复元素”。试试把list放入set，看看是否会报错。


#再议不可变对象
#上面我们讲了，str是不变对象，而list是可变对象。

#对于可变对象，比如list，对list进行操作，list内部的内容是会变化的，比如：

a = ['c', 'b', 'a']
a.sort()
print(a)
#['a', 'b', 'c']

#而对于不可变对象，比如str，对str进行操作呢：

a = 'abc'
a.replace('a', 'A')
#'Abc'
print(a)
#'abc'

#虽然字符串有个replace()方法，也确实变出了'Abc'，但变量a最后仍是'abc'，应该怎么理解呢？

#我们先把代码改成下面这样：

a = 'abc'
b = a.replace('a', 'A')
print(b)
#'Abc'
print(a)
#'abc'

#要始终牢记的是，a是变量，而'abc'才是字符串对象！有些时候，我们经常说，对象a的内容是'abc'，但其实是指，a本身是一个变量，它指向的对象的内容才是'abc'：

#当我们调用a.replace('a', 'A')时，实际上调用方法replace是作用在字符串对象'abc'上的，而这个方法虽然名字叫replace，但却没有改变字符串'abc'的内容。相反，replace方法创建了一个新字符串'Abc'并返回，如果我们用变量b指向该新字符串，就容易理解了，变量a仍指向原有的字符串'abc'，但变量b却指向新字符串'Abc'了：

#所以，对于不变对象来说，调用对象自身的任意方法，也不会改变该对象自身的内容。相反，这些方法会创建新的对象并返回，这样，就保证了不可变对象本身永远是不可变的。


#练习题:验证tuple对象放入dict 或者set
a=set(1,2,3)
a=set(1,[2,3])



