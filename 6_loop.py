#!/usr/local/bin/python3
#coding=utf-8
#循环
#for 的用法
names = ['Michael', 'Bob', 'Tracy']
for name in names:
    print(name)

sum = 0
for x in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]:
    sum = sum + x
print(sum)

sum = 0
for x in range(101):	#range()函数生成1-100的字0--100的字符串
	sum = sum + x
print(sum)


#while的用法
sum = 0
n = 99
while n > 0:
    sum = sum + n
    n = n - 2
print(sum)

#练习题:请利用循环依次对list中的每个名字打印出Hello, xxx!
L = ['Bart', 'Lisa', 'Adam']
for hello in L:
	print('hello ,',hello)



#break的用法
n = 1
while n <= 100:
    print(n)
    n = n + 1
print('END')

# 当n = 11时，条件满足，执行break语句,break语句会结束当前循环
n = 1
while n <= 100:
    if n > 10:
        break
    print(n)
    n = n + 1
print('END')

#continue 的用法
n = 0
while n < 10:
    n = n + 1
    print(n)


# 如果n是偶数，执行continue语句,continue语句会直接继续下一轮循环，后续的print()语句不会执行
n = 0
while n < 10:
    n = n + 1
    if n % 2 == 0:
        continue
    print(n)

#break语句可以在循环过程中直接退出循环，而continue语句可以提前结束本轮循环，并直接开始下一轮循环。
#这两个语句通常都必须配合if语句使用。

#练习题:99乘法口诀


#1.长方形完整格式
for a in range(1,10):
	for b in range(a,10):
		print(a,'*',b,'=',a * b,end=" ")
	print("")
		



#2.左上三角形
for a in range(1,10):
    for b in range(1,a+1):
        print(a,'*',b,'=',a * b,end=" ")
    print("")

#3.右上三角形
for i in range(1,10):
    for k in range(1,i):
        print (end="       ")
    for j in range(i,10):
            print("%d*%d=%2d" % (i,j,i*j),end=" ")
    print("")



#4.左下三角形

for a in range(1,10):
    for b in range(1,a+1):
        print(a,'*',b,'=',a * b,end=" ")
    print("")



#5.右下三角形
for i in range(1,10):
    for k in range(1,10-i):
        print(end="       ")
    for j in range(1,i+1):
        product=i*j
        print("%d*%d=%2d" % (i,j,product),end=" ")
    print (" ")






