#!/usr/local/bin/python3
#coding = utf-8
#class后面紧接着是类名，类名通常是大写开头的单词，紧接着是(object)，表示该类是从哪个类继承下来的，如果没有合适的继承类，就使用object类，这是所有类最终都会继承的类。
class Student(object):
    pass

#定义好了Student类，就可以根据Student类创建出Student的实例，创建实例是通过类名+()实现的：

bart = Student()
bart
#<__main__.Student object at 0x10a67a590>
Student
#<class '__main__.Student'>

#可以看到，变量bart指向的就是一个Student的实例，后面的0x10a67a590是内存地址，每个object的地址都不一样，而Student本身则是一个类。
#可以自由地给一个实例变量绑定属性，比如，给实例bart绑定一个name属性：

bart.name = 'Bart Simpson'
bart.name
#'Bart Simpson'

#通过定义一个特殊的__init__方法，在创建实例的时候，就把name，score等属性绑上去：


class Student(object):
    def __init__(self, name, score):
        self.name = name
        self.score = score
#__init__方法的第一个参数永远是self，表示创建的实例本身，因此，在__init__方法内部，就可以把各种属性绑定到self，因为self就指向创建的实例本身。

#有了__init__方法，在创建实例的时候，就不能传入空的参数了，必须传入与__init__方法匹配的参数，但self不需要传，Python解释器自己会把实例变量传进去：

bart = Student('Bart Simpson', 59)
bart.name
#'Bart Simpson'
bart.score
#59
#和普通的函数相比，在类中定义的函数只有一点不同，就是第一个参数永远是实例变量self，并且，调用时，不用传递该参数。

#数据封装:面向对象的重要特点是数据封装，student类中，每个实例拥有各执的name,score。用于通过函数访问。
def print_score(std):
    print('%s: %s' % (std.name, std.score))

print_score(bart)

#直接将函数封装在类中:函数将成为类中的一个方法

class Student(object):
    def __init__(self, name, score):	#类的方法，用于类调用时传入参数，以及将传参赋值给相应的对象
        self.name = name
        self.score = score

    def print_score(self):	#定义类中的方法,第一个参数是self,其他的和普通函数一样,调用时正常传入其他参数
        print('%s: %s' % (self.name, self.score))

bart = Student('Bart Simpson', 59)

bart.print_score()

#add new method in class Student convenient.
class Student(object):

    def __init__(self, name, score):
        self.name = name
        self.score = score

    def print_score(self):
        print('%s: %s' % (self.name, self.score))

    def get_grade(self):
        if self.score >= 90:
            return 'A'
        elif self.score >= 60:
            return 'B'
        else:
            return 'C'

bart = Student('Bart Simpson', 59)
bart.get_grade()

#类是创建实例的模板，而实例则是一个一个具体的对象，各个实例拥有的数据都互相独立，互不影响；
#class is the module to create instance.
#方法就是与实例绑定的函数，和普通函数不同，方法可以直接访问实例的数据；
#通过在实例上调用方法，我们就直接操作了对象内部的数据，但无需知道方法内部的实现细节。
#继承和多态

#访问限制


#定义一个Animal类:
class Animal(object):
    def run(self):
        print('Animal is running...')

class Dog(Animal):  	 #Dog类 直接继承Animal类,Animal是Dog的父类,可以继承他所有的属性
    pass

class Cat(Animal):	 #Cat类 也从Animal类继承
    pass

dog = Dog()		#定义dog对象为Dog类
dog.run()		#调用Dog对象中的run()属性

cat = Cat()
cat.run()

class Dog(Animal):			#定义Dog类集成Animal类属性,增加子类属性(方法)
    def run(self):			#定义run方法
        print('Dog is running....')

    def eat(self):			#定义eat方法
        print('Eating meat....')


class Dog(Animal):			#改进子类,分类打印,自父同时存在某方法,子会覆盖父,多态.
    def run(self):
        print('Dog is running...')

class Cat(Animal):
    def run(self):
        print('Cat is running')


a = list() # a是list类型
b = Animal() # b是Animal类型
c = Dog() # c是Dog类型

isinstance(a, list)
#True
isinstance(b, Animal)
#True
isinstance(c, Dog)
#True

isinstance(c, Animal)
#True

b = Animal()
isinstance(b, Dog)
#False


def run_twice(animal):
    animal.run()
    animal.run()

run_twice(Animal())
run_twice(Dog())
run_twice(Cat)

class Tortoise(Animal):		#新增一个Animal的子类，不必对run_twice()做任何修改，任何依赖Animal作为参数的函数或者方法都可以不加修改地正常运行，原因就在于多态。
    def run(self):
        print('Tortoise is running slowly...')

run_twice(Tortoise())


#多态的好处就是，当我们需要传入Dog、Cat、Tortoise……时，我们只需要接收Animal类型就可以了，因为Dog、Cat、Tortoise……都是Animal类型，然后，按照Animal类型进行操作即可。由于Animal类型有run()方法，因此，传入的任意类型，只要是Animal类或者子类，就会自动调用实际类型的run()方法，这就是多态的意思：

#对于一个变量，我们只需要知道它是Animal类型，无需确切地知道它的子类型，就可以放心地调用run()方法，而具体调用的run()方法是作用在Animal、Dog、Cat还是Tortoise对象上，由运行时该对象的确切类型决定，这就是多态真正的威力：调用方只管调用，不管细节，而当我们新增一种Animal的子类时，只要确保run()方法编写正确，不用管原来的代码是如何调用的。这就是著名的“开闭”原则：
#对扩展开放：允许新增Animal子类；
#对修改封闭：不需要修改依赖Animal类型的run_twice()等函数。


#静态语言 vs 动态语言
#对于静态语言（例如Java）来说，如果需要传入Animal类型，则传入的对象必须是Animal类型或者它的子类，否则，将无法调用run()方法。

#对于Python这样的动态语言来说，则不一定需要传入Animal类型。我们只需要保证传入的对象有一个run()方法就可以了：

class Timer(object):
    def run(self):
        print('Start...')
#这就是动态语言的“鸭子类型”，它并不要求严格的继承体系，一个对象只要“看起来像鸭子，走起路来像鸭子”，那它就可以被看做是鸭子。

#Python的“file-like object“就是一种鸭子类型。对真正的文件对象，它有一个read()方法，返回其内容。但是，许多对象，只要有read()方法，都被视为“file-like object“。许多函数接收的参数就是“file-like object“，你不一定要传入真正的文件对象，完全可以传入任何实现了read()方法的对象。

#定义一个父类一个子类
class Province(object):
    def __init__(self,proname):
        self.proname=proname
    def ps(self):
        print('I am in %s'%self.proname)

class City(Province):
    def __init__(self,proname,cityname):
        self.cityname=cityname
        Province.__init__(self,proname)
    def ps1(self):
        print('I\'m in %s-%s' %(self.proname,self.cityname))

#定义一个独立的类
class Timer(object):
    def ps(self):
        print('我不属于Province类或其子类，但我有ps方法我同样可以被调用')
    def ps1(self):
        print('我不属于Province类或其子类，但我有ps1方法我同样可以被调用')

#定义一个函数
def f(x):
    x.ps()
    x.ps1()


#调用部分
f(City('上海','浦东'))
f(Timer())			#鸭子类型


