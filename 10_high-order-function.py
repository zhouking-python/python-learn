#/usr/local/bin/python3
#函数是Python内建支持的一种封装，我们通过把大段代码拆成函数，通过一层一层的函数调用，就可以把复杂任务分解成简单的任务，这种分解可以称之为面向过程的程序设计。函数就是面向过程的程序设计的基本单元。
#而函数式编程（请注意多了一个“式”字）——Functional Programming，虽然也可以归结到面向过程的程序设计，但其思想更接近数学计算。
#我们首先要搞明白计算机（Computer）和计算（Compute）的概念。
#在计算机的层次上，CPU执行的是加减乘除的指令代码，以及各种条件判断和跳转指令，所以，汇编语言是最贴近计算机的语言。
#而计算则指数学意义上的计算，越是抽象的计算，离计算机硬件越远。
#对应到编程语言，就是越低级的语言，越贴近计算机，抽象程度低，执行效率高，比如C语言；越高级的语言，越贴近计算，抽象程度高，执行效率低，比如Lisp语言。
#函数式编程就是一种抽象程度很高的编程范式，纯粹的函数式编程语言编写的函数没有变量，因此，任意一个函数，只要输入是确定的，输出就是确定的，这种纯函数我们称之为没有副作用。而允许使用变量的程序设计语言，由于函数内部的变量状态不确定，同样的输入，可能得到不同的输出，因此，这种函数是有副作用的。
#函数式编程的一个特点就是，允许把函数本身作为参数传入另一个函数，还允许返回一个函数！
#Python对函数式编程提供部分支持。由于Python允许使用变量，因此，Python不是纯函数式编程语言。

#变量可以指向函数
abs(-10)		#求绝对值
abs			#abs(-10)调用函数,abs函数本身
x = abs(-10)		#赋值函数运行的结果给变量x
print(x)

f = abs			#函数赋值给变量f,可以通过变量来调用
print(f(-10))		#说明变量f现在已经指向了abs函数本身。直接调用abs()函数和调用变量f()完全相同.

#函数名也是变量:函数名其实就是指向函数的变量!对于abs()函数,函数名abs看成变量,它指向一个可以计算绝对值的变量.
abs = 10
abs(-10)		#报错,要让修改abs变量的指向在其它模块也生效,要用import builtins; builtins.abs = 10.


#传入函数:既然变量可以指向函数,函数的参数能接收变量,那么一个函数就可以接收另一个函数作为参数,这种函数就称之为高阶函数。
def add(x, y, f):
    return f(x) + f(y)

add(-5, 6, abs) 	#函数add调用函数abs

##把函数作为参数传入，这样的函数称为高阶函数，函数式编程就是指这种高度抽象的编程范式


<<<<<<< HEAD
#map & reduce :	map()函数接收两个参数，一个是函数，一个是Iterable，map将传入的函数依次作用到序列的每个元素，并把结果作为新的Iterator返回。
=======
#map/reduce:python内建了map() and reduce()函数
#map将传入的函数依次作用到序列的每个元素，并把结果作为新的Iterator(迭代器)返回
def f(x):
    return x * x

r=map(f, [1, 2, 3, 4, 5, 6, 7, 8, 9])	#map()传入的第一个参数为f函数本身，会循环将后面的数据执行一遍函数运算
list(r)				#转换为Iterator(迭代器，可用循环调用)

for i in r:			#转换为Iterator(迭代器，可用循环调用)
    print(i)


L = []				#可以使用个循环也能得到同样的效果
for n in list(range(1,10)):
    L.append(f(n))
print(L)


list(map(str, [1, 2, 3, 4, 5, 6, 7, 8, 9]))	#map()传入第一个str函数将后面的数据输入至函数执行变换为字符串

#reduce把一个函数作用在序列[x1, x2, x3, ...]上,这个函数必须接收两个参数,reduce把结果继续和序列的下一个元素做累积计算，其效果如下:
reduce(f, [x1, x2, x3, x4]) = f(f(f(x1, x2), x3), x4)

from functools import reduce
    def add(x, y):
        return x + y

reduce(add,[1,3,5,7,9])


from functools import reduce
def fn(x, y):
    return x * 10 + y

reduce(fn,[1, 3, 5, 7, 9])

from functools import reduce
def fn(x, y):
    return x * 10 + y


>>>>>>> d34b15f737dc5064213b0c26d1c5cb15355a65f5

def f(x):
    return x * x

r = map(f,list(range(1,10)))	#map()将传入的list元素遍历执行运算后赋值给r
print(r)


L = []
for n in [1, 2, 3, 4, 5, 6, 7, 8, 9]:	#使用循环将list中的每一个元素传入函数进行运算后赋值传入L
    L.append(f(n))
print(L)

list(map(str, [1, 2, 3, 4, 5, 6, 7, 8, 9]))	#map()函数将list中元素遍历执行str()函数后输出


#reduce把一个函数作用在一个序列[x1, x2, x3, ...]上，这个函数必须接收两个参数，reduce把结果继续和序列的下一个元素做累积计算。

reduce(f, [x1, x2, x3, x4]) = f(f(f(x1, x2), x3), x4)

#序列求和
from functools import reduce
def add(x, y):
    return x + y

reduce(add,list(range(1,10,2)))
reduce(add,[1, 3, 5, 7, 9])
sum(list(range(1,10,2)))
###########################################
from functools import reduce
def fn(x, y):
    return x * 10 + y

reduce(fn, [1, 3, 5, 7, 9])		#将1,3,5,7,9转换为13579
reduce(fn, list(range(1,10,2)))

############################################
from functools import reduce
def fn(x, y):
    return x * 10 + y

def char2num(s):			#将传入的的srt转换为int
    return {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}[s]

reduce(fn, map(char2num, '13579'))
###############################################

from functools import reduce

def str2int(s):
    def fn(x, y):
        return x * 10 + y
    def char2num(s):
        return {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}[s]
    return reduce(fn, map(char2num, s))
##############################################

from functools import reduce

def char2num(s):
    return {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}[s]

def str2int(s):
    return reduce(lambda x, y: x * 10 + y, map(char2num, s))	#lambda()
################################################

#练习题:利用map()函数，把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']：


def normalize(name):
    name=str(name).lower().capitalize()   #lower() turn all str to  lower,capitalize() turn the first srt to upper
    return name

L1 = ['adam', 'LISA', 'barT']
L2 = list(map(normalize, L1))


#请编写一个prod()函数，可以接受一个list并利用reduce()求积

from functools import reduce
def prod(x, y):
    return x * y

reduce(prod,[1,3,5,7,9])


#利用map和reduce编写一个str2float函数，把字符串'123.456'转换成浮点数123.456:

from functools import reduce
def str2float(s):
    s=float(s)
    return s


 from functools import reduce
def str2float(s):
    def fn(x,y):
        return x*10+y
    n=s.index('.')
    s1=list(map(int,[x for x in s[:n]]))
    s2=list(map(int,[x for x in s[n+1:]]))
    return reduce(fn,s1) + reduce(fn,s2)/10**len(s2)
print('\'123.4567\'=',str2float('123.4567'))   
