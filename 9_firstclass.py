#!/usr/local/bin/python3
#coding=utf-8
#example
#biult a list  [ 1, 3 ,... 99 ]
L = [ ]
n = 1
while n <= 99:
	L.append(n)
	n = n + 2


#切片:取出tuple 或者list中的某些元素
L  = ['Michael', 'Sarah', 'Tracy', 'Bob', 'Jack']

#取前三个元素
print([L[0], L[1], L[2]])

#取前N个元素
r = []
n = 3
for  i in range(n):
	r.append(L[i])

print(r)


#循环取指定索引范围的操作循环十分繁琐,Python提供了切片（Slice）操作符,可大大简化这种操作.
print(L[0:3]) 		#索引从第一个元素开始到3止不含3.
print(L[:3])  		#索引从头开始到第2个元素止不含3.
print(L[1:3]) 		#索引从第二个元素开始到第三个元素结束。
print(L[-2:]) 		#索引从倒数第二个元素开始到最后一个结束。
print(L[-2:-1]) 	#索引从倒数第二个元素开始到倒数第一个结束，但不包含倒数第一个。

#利用函数创建数列
L = list(range(100))	#rang函数生成a-b的对象用法,list函数按照对象进入的顺序保存对象到一个集合中
print(L)

print(L[:10])			#切片前10个数
print(L[-10:])			#切片后10个数
print(L[10:20])			#切片第10个数到第19个数
print(L[:10:2])			#前10位每两个取一个
print(L[::5])			#所有数,每5个取一个
print(L[:])				#直接打印全部元素

print((0, 1, 2, 3, 4, 5)[:3])	 #tuple虽然不可变，也可以做切片操作,操作的结果不会改变tuple的值
print('ABCDEFG'[:3])			 #字符串也是一种list,每个字符是一个元素,切片操作后字符串不变
print('ABCDEFG'[::2])



#迭代:list或tuple 可以通过for循环来遍历,这种遍历我们称之为迭代(iteration)
d = {'a': 1, 'b': 2, 'c': 3}
for key in d:				#循环打印dict d中key的值
	print(key)


for value in d.values():	#循环打印dict d中values的值
	print(value)

for k, v in d.items():		#循环打印 dict d 中kv的值
	print(k,v)

for ch in 'abc':
	print(ch)				#字符串也可以用与迭代对象,循环打印abc

#判断一个对象是否可迭代,通过导入collections模块的iterable类型:
from collections import Iterable			#导入模块
print(isinstance('abc', Iterable))			#str是否可迭代
print(isinstance([1,2,3], Iterable))		#list是否可迭代
print(isinstance(123, Iterable))			#整数是否可迭代

#实现list索引值与元素对应的循环
for i, value in enumerate(['A', 'B', 'C']):
	print(i, value)


#同时引用两个变量的循环
for x, y in [(1, 1), (2, 4), (3, 9)]:
	print(x, y)

#列表生成式:List Comprehensions,是python内置的非常简单却强大的用来创建list的生成式.
print(list(range(1, 11))) #生成[1..10]

L = []					  #使用循环生成[1*1,2*2..,10*10]
for x in  range(1,11):
	L.append(x * x)

print(L)

L=[x * x for x in range(1, 11)]	#使用列表生成式生成[1*1,2*2..,10*10],生成的元素x * x放到前面后面跟for循环
print(L)
L=[x * x for x in range(1, 11) if x % 2 == 0] #if条件加入筛选出2的倍数
print(L)

L=[m + n for m in 'ABC' for n in 'XYZ'] #两层循环,生成全排列
print(L)


import os
L=[d for d in os.listdir('.')] #os.listdir可以列出文件和目录
print(L)



d = {'x': 'A', 'y': 'B', 'z': 'C' }
for k, v in d.items():			#for循环可同时使用两个甚至多个变量,dict的items()可以同时迭代key和value：
	print(k, '=', v)


d = {'x': 'A', 'y': 'B', 'z': 'C' }
[k + '='  + v for k, v in d.items()]	#列表生成式可以使用两个变量来生成list。

L = ['Hello', 'World', 'IBM', 'Apple']
print([s.lower() for s in L])			#列表生成式将所有的列字符变成小写.

L = ['Hello', 'World', 18, 'Apple', None]
[ s.lower()  for s in L if isinstance(s, str) ] # isinstance(s, str) 判断为真才执行

# 生成器:在Python中，这种一边循环一边计算的机制，称为生成器：generator.
L = [x * x for x in range(10)]
g = (x * x for x in range(10))	#把一个列表生成式的[]改成()，就创建了一个generator。
next(g)
g = (x * x for x in range(10))	#创建声称其后，通过for循环来迭代输出
for n in g:
	print(n)

#的斐波拉契数列（Fibonacci），除第一个和第二个数外，任意一个数都可由前两个数相加得到
#1, 1, 2, 3, 5, 8, 13, 21, 34, ...
def fib(max):
    n, a, b = 0, 0, 1
    while n < max:
        print(b)
        a, b = b, a + b
        n = n + 1
    return 'done'

#赋值语句a, b = b, a + b
#t = (b, a + b) # t是一个tuple
#a = t[0]
#b = t[1]

#如果一个函数定义中包含yield关键字，那这个函数就不是普通函数，而是一个generator
def fib(max):
	n, a, b = 0, 1, 1
	while n < max:
		a, b = b, a+b
		yield b
		n = n + 1
	return 'done'
#含yield的函数和一般函数的区别:
####函数是顺序执行，遇到return语句或者最后一行函数语句就返回。而变成generator的函数，在每次调用next()的时候执行，遇到yield语句返回，再次执行时从上次返回的yield语句处继续执行。


for i in fib(6):
	print(i)

g = fib(6)
while True:
	try:
		x = next(g)
		print('g:', x)
	except StopIteration as e:
		print('Generator return value:', e.value)
		break

#杨辉三角
# [1]
# [1, 1]
# [1, 2, 1]
# [1, 3, 3, 1]
# [1, 4, 6, 4, 1]
# [1, 5, 10, 10, 5, 1]
# [1, 6, 15, 20, 15, 6, 1]
# [1, 7, 21, 35, 35, 21, 7, 1]
# [1, 8, 28, 56, 70, 56, 28, 8, 1]
# [1, 9, 36, 84, 126, 126, 84, 36, 9, 1]


def triangles():
	L = [1]
	while True:
		yield L
		L=[L[x]+L[x+1] for x in range(len(L)-1)]	#核心计算方式[L[x] + L[x+1] for x in range(len(L)-1)]
		L.insert(0,1)								#在0的位置增加数据为1的list值
		L.append(1)									#在最末端增加数据为1的list值
		if len(L)>10:  								#len(L) > 10退出循环
			break

#可以直接作用于for循环的数据类型有以下几种:
#一类是集合数据类型 list、tuple、dict、set、str等；
#一类是generator，包括生成器和带yield的generator function。
#这些可以直接作用于for循环的对象统称为可迭代对象：Iterable。
#可以使用isinstance()判断一个对象是否是Iterable对象.

from collections import Iterable
isinstance ([],Iterable)
isinstance ({},Iterable)
isinstance ('abc',Iterable)
isinstance (x for x in range(10), Iterable)
isinstance (100, Iterable)


#而生成器不但可以作用于for循环，还可以被next()函数不断调用并返回下一个值，直到最后抛出StopIteration错误表示无法继续返回下一个值了。
#可以被next()函数调用并不断返回下一个值的对象称为迭代器：Iterator。
#可以使用isinstance()判断一个对象是否是Iterator对象.
from collections import Iterator
isinstance((x for x in range(10)), Iterator)
isinstance([], Iterator)
isinstance({}, Iterator)
isinstance('abc', Iterator)


#生成器都是Iterator对象，但list、dict、str虽然是Iterable，却不是Iterator。
#把list、dict、str等Iterable变成Iterator可以使用iter()函数.
isinstance(iter([]), Iterator)
isinstance(iter('aaa'), Iteratr)

#为什么list、dict、str等数据类型不是Iterator？

#这是因为Python的Iterator对象表示的是一个数据流，Iterator对象可以被next()函数调用并不断返回下一个数据，直到没有数据时抛出StopIteration错误。可以把这个数据流看做是一个有序序列，但我们却不能提前知道序列的长度，只能不断通过next()函数实现按需计算下一个数据，所以Iterator的计算是惰性的，只有在需要返回下一个数据时它才会计算。
#Iterator甚至可以表示一个无限大的数据流，例如全体自然数。而使用list是永远不可能存储全体自然数的。

#1.凡是可作用于for循环的对象都是Iterable类型；
#2.凡是可作用于next()函数的对象都是Iterator类型，它们表示一个惰性计算的序列
#3.集合数据类型如list、dict、str等是Iterable但不是Iterator，不过可以通过iter()函数获得一个Iterator对象。


##python的for循环本质上就是通过不断调用next()函数实现的，例如:
for x in [1, 2, 3, 4, 5]:
    pass
#实际上完全等价于：
# 首先获得Iterator对象:
it = iter([1, 2, 3, 4, 5])
# 循环:
while True:
    try:
        # 获得下一个值:
        x = next(it)
    except StopIteration:
        # 遇到StopIteration就退出循环
        break

