#!/usr/local/python3.6/bin/python3
#coding=utf-8
#title:数据类型和变量
#数据类型:整数:(1,100.-100,0)、浮点数:(1.23,3.14,-9.01,1.23x109=1.23e9,0.000012=1.2e-5)
#		  字符串:以单引号'或双引号"括起来的任意文本,转义字符\可以转义很多字符，比如\n表示换行，\t表示制表符，字符\本身也要转义，所以\\表示的字符就是\,r''表示''内部的字符串默认不转义.
print('I\'m ok.','I\'m learning\nPython.','\\\n\\',r'\\\t\\','''line1
line2
line3''')
#		  布尔值
True
False
3 > 2
3 > 5
True and True
True and False
False and False
5 > 3 and 3 > 1
True or True
True or False
False or False
5 > 3 or 1 > 3
not True
not False
not 1 > 2
if age >= 18:
	print('adult')
else:
	print('teenager')
#		  空值:空值是Python里一个特殊的值，用None表示。None是一个特殊的空值。
#		  变量:变量不仅可以是数字，还可以是任意数据类型,变量名必须是大小写英文、数字和_的组合，且不能用数字开头.
a = 1 			#a整数变量
t_007 = 'T007' 	#变量t_007是一个字符串
Answer = True	#变量Answer是一个布尔值True.
#等号=是赋值语句，可以把任意数据类型赋值给变量，同一个变量可以反复赋值，而且可以是不同类型的变量
a = 123 # a是整数
print(a)
a = 'ABC' # a变为字符串
print(a)

#以下=为赋值语句
x = 10
x = x + 2

#解释器工作流程
a = 'ABC'	#1.在内存中创建了一个'ABC'的字符串;2.在内存中创建了一个名为a的变量，并把它指向'ABC'.
b = a		#3.内存中创建变量b,并i将变量b指向变量a指向的数据(ABC);
a = 'XYZ'	#4.在内存中创建了一个'XYZ'的字符串;2.将a变量重新指向'XYZ'.
print(b)	#打印b变量b为ABC.

#		  常量:常量就是不能变的变量,常用的数学常数π就是一个常.Python中，通常用全部大写的变量名表示常量.
PI = 3.14159265359	#可变,py并没有机制保障它不可变.
print(10 / 3)		#/除法计算结果是浮点数,即使是两个整数恰好整除,结果也是浮点数,
print(9 / 3)
print(10 // 3)		#整数的地板除//永远是整数
print(10 % 3)		#无论整数做//除法还是取余数,结果永远是整数,所以,整数运算结果永远是精确的.




#练习题:请打印出以下变量的值：
n = '123'
f = '456.789'
s1 = 'Hello, world'
s2 = 'Hello, \'Adam\''
s3 = r'Hello, "Bart"'
s4 = r'''Hello,
Lisa!'''


print(n,f,s1,s2,s3,s4)
