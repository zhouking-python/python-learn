#!/usr/local/python3.6/bin/python3
#coding=utf-8
#filter()把传入的函数依次作用于每个元素，然后根据返回值是True还是False决定保留还是丢弃该元素。实现筛选函数。
def is_odd(n):
    return n % 2 == 1

list(filter(is_odd, [1, 2, 4, 5, 6, 9, 10, 15]))


def not_empty(s):
    return s and s.strip()

list(filter(not_empty, ['A', '', 'B', None, 'C', '  ']))

#filter求素数:
#1.构造从2开始的自然数列:2,3,4,5,6,7,8,9,10,...
#2.取序列的第一个数2,他一定是一个素数,然后用2 把序列的2的倍数删掉.
#3.新序列的第一个数3,他一定是素数,然后用3把数列三的倍数删掉.
#3.取新序列的第一个数5,他一定是一个素数,然后用5把序列中5的倍数删掉
# 循环......

#构造从3开始的奇数序列:生成器生成一个无线序列
def _odd_iter():
    n = 1
    while True:
        n = n + 2
	yield n

#定义删选函数:
def _not_divisible(n):
    return lambda x: x % n > 0

#定义生成器,不断返回下一个素数:生成器先返回第一个素数,然后利用filter()不断产生删选后的新序列.
def primes():
    yield 2
    it = _odd_iter()
    while True:
        n = next(it)
        yield n
        it = filter(_not_divisible(n), it)

#调用时设置退出循环条件:
for n in primes():
    if n < 1000:
         print(n)
    else:
	 break


#左向右读和从右向左读都是一样的数,例如12321,909.请利用filter()滤掉非回数.
def _odd_iter():
    n = 1
    while True:
        n = n + 1
        yield n

for n in _odd_iter():
        n = str(n)
        if str(n) != str(n[::-1]) and n < 1000:
            print(n)
    	elif str(n) == str(n[::-1]) and n < 1000:
            print('hello')
        else:
            break

def is_palindrome(n):
    return lambda n: str(n) != str(n)[::-1]



def is_palindrome(n):
    if str(n) != str(n)[::-1]:
        return n

output = filter(is_palindrome, range(1, 1000))
print(list(output))



#sorted函数用法:排序算法:key指定的函数将作用于list的每一个元素上,并根据key函数返回的结果进行排序
sorted([36, 5, -12, 9, -21])		#将字符或者数字进行排列
sorted([36, 5, -12, 9, -21], key=abs)	#接收key函数实现自定义排序,abs按绝对值大小排序,源字符不变

sorted(['bob', 'about', 'Zoo', 'Credit']) #将字符串按照ASCII的大小排列
sorted(['bob', 'about', 'Zoo', 'Credit'], key=str.lower)	#都变更为小写,排列准确

sorted(['bob', 'about', 'Zoo', 'Credit'], key=str.lower, reverse=True)  #增加反向排序


#练习:用一组tuple表示学生名字和成绩.L = [('Bob', 75), ('Adam', 92), ('Bart', 66), ('Lisa', 88)]
#请用sorted()对上述列表分别按名字排序.

L = [('Bob', 75), ('Adam', 92), ('Bart', 66), ('Lisa', 88)]
def by_name(t):
    L=str(t).lower()
    return L

L2 = sorted(L, key=by_name)
print(L2)


L = [('Bob', 75), ('Adam', 92), ('Bart', 66), ('Lisa', 88)]
sorted(L, key=lambda L : L[1] ,reverse=True)




    
