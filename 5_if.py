#!/usr/local/bin/python3
#coding=utf-8
#条件判断
age = 20
if age >= 18:
    print('your age is', age)
    print('adult')

age = 3
if age >= 18:
    print('your age is', age)
    print('adult')
else:
    print('your age is', age)
    print('teenager')


age = 3
if age >= 18:
    print('adult')
elif age >= 6:
    print('teenager')
else:
    print('kid')

###语法规则######
#if <条件判断1>:
#    <执行1>
#elif <条件判断2>:
#    <执行2>
#elif <条件判断3>:
#    <执行3>
#else:
#    <执行4>

age = 20
if age >= 6:			#此处已经匹配,不再往下匹配
    print('teenager')
elif age >= 18:
    print('adult')
else:
    print('kid')


birth = input('birth: ') #此处定义为字符串
if int(birth) < 2000:	 #int()将变量转变为整形,方可进行布尔运算
    print('00前')
else:
    print('00后')

#练习题:小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，
#并根据BMI指数：
#低于18.5：过轻
#18.5-25：正常
#25-28：过重
#28-32：肥胖
#高于32：严重肥胖


height = 1.75
weight = 80.5
bmi = 80.5 / 1.75

if bmi < 18.5:
	print('xiao ming is to light')
elif 18.5 <= bmi <= 25:
	print('xiao ming is normal')
elif 25 < bmi < 28:
	print('xiao ming is weight')
elif 28 <= bmi <= 32:
	print('xiao ming is too weight')
else:
	print('xiao ming is too fat')
