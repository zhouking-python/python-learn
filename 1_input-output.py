#!/usr/local/bin/python3
#coding=utf-8
#print()括号内加上字符串,会在屏幕上输出该字符串.
print('hello, world')
#print()函数也可以接受多个字符串，用逗号","隔开',逗号输出后会成为空格，就可以连成一串输出.
print('The quick brown fox', 'jumps over', 'the lazy dog')
#print()也可以打印整数，或者计算结果：
print(300)
print(100 + 200)
print('100 + 200 =', 100 + 200)

#输入:name = input()并按下回车后,Python交互式命令行就在等待你的输入了
name = input()	#会将输入的字符串作为变量存放在变量名为name的变量中.

name = input('please enter your name: ')
print('hello,', name)


#练习题:print()输出1024 * 768 = xxx:
print('1024 * 768 =',1024 * 768)
